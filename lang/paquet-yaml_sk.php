<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-yaml?lang_cible=sk
// ** ne pas modifier le fichier **

return [

	// Y
	'yaml_description' => 'Tento zásuvný modul ponúka funkcie čítanie/zápisu formátu YAML:
	<code>yaml_decode()</code> a <code>yaml_encode()</code>. Poskytuje formát yaml aj pre cyklus (DATA).',
	'yaml_slogan' => 'Jednoduchý formát súborov na upravovanie zoznamov dát',
];
