<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-yaml?lang_cible=mg
// ** ne pas modifier le fichier **

return [

	// Y
	'yaml_description' => 'Ce plugin fournit les fonctions de lecture/écriture du format YAML :
	<code>yaml_decode()</code> et <code>yaml_encode()</code>. Il fournit aussi le format yaml pour la boucle (DATA).',
	'yaml_slogan' => 'Un format de fichier simple pour éditer des listes de données',
];
