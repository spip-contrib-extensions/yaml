<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-yaml?lang_cible=de
// ** ne pas modifier le fichier **

return [

	// Y
	'yaml_description' => 'Dieses Plugin stellt die Funtkionen zum lesen/schreiben des Formats YAML bereit :
	<code>yaml_decode()</code> und <code>yaml_encode()</code>. Es ermöglicht die Verwendung des Formats YAML in Schleifen (DATA).',
	'yaml_slogan' => 'Ein einfaches Dateiformat um Datenlisten zu bearbeiten',
];
