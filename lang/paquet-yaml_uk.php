<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-yaml?lang_cible=uk
// ** ne pas modifier le fichier **

return [

	// Y
	'yaml_description' => 'Цей плагін надає функції для читання/запису у форматі YAML:
<code>yaml_decode()</code> та <code>yaml_encode()</code>.

Він також додає підтримку формату YAML для циклу (DATA).',
	'yaml_slogan' => 'Простий формат файлів для редагування списків даних',
];
