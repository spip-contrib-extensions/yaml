<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-yaml?lang_cible=it
// ** ne pas modifier le fichier **

return [

	// Y
	'yaml_description' => 'Questo plugin fornisce le funzioni di lettura/scrittura del formato YAML
	<code>yaml_decode()</code> e <code>yaml_encode()</code>.  Fornisce anche il formato yaml per il ciclo (DATA).',
	'yaml_slogan' => 'Un semplice formato file per modificare elenchi di dati',
];
