<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-yaml?lang_cible=fa
// ** ne pas modifier le fichier **

return [

	// Y
	'yaml_description' => 'اين پلاگين كاركردهاي خواندن/نوشتن فرمت يا.اي.ام.ال را فراهم مي‌آورد: <code>yaml_decode()</code> و<code>yaml_encode()</code>. همچنين فرمت يا.اي.ام.ال را براي حلقه‌ي (DATA) فراهم مي‌سازد.',
	'yaml_slogan' => 'فرمتي براي پروند‌ه ساده براي ويرايش فهرست‌هاي داده‌ها',
];
